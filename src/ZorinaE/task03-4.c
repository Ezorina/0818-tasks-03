#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#define	n	5
#define	m	50

int main()
{
	char arr[n][m];
	int i=0,j=0,random[n],temp,k;
	puts("Enter strings:");
	while (i<n)
	{
		fgets(arr[i], m, stdin);
		if (arr[i][0]=='\n')
			break;
		i++;
		j++;
	}
	srand(time(0));
	printf("\n");
	for (i=0; i<j; i++)
		random[i]=i;
	for (i=0; i<j; i++)
	{
		k=rand()%j;
		temp=random[i];
		random[i]=random[k];
		random[k]=temp;
	}
	for(i=0;i<j;i++)
		printf("%s", arr[random[i]]);
	return 0;
}