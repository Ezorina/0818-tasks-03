#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#define	n	5
#define	m	50

int main()
{
	char arr[n][m];
	int i=0,j=0;
	puts("Enter strings:");
	while (i<n)
	{
		fgets(arr[i], m, stdin);
		if (arr[i][0]=='\n')
			break;
		i++;
		j++;
	}
	srand(time(0));
	printf("\n");
	i=rand()%j;
	printf("%s\n", arr[i]);
	return 0;
}